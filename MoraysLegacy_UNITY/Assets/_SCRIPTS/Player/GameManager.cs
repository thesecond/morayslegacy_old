﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{


    public static GameManager staticInstance;

    public Camera_Controller cameraController;
    public SaveLoad_Controller saveLoadController;


    #region DELETE LATER!
    

    #endregion

    private void Awake()
    {
        if(staticInstance == null)
        {
            staticInstance = this;
        } else {
            Destroy(gameObject);
        }

        saveLoadController = new SaveLoad_Controller();


        Camera_Model cameraModel = new Camera_Model();
        cameraController = new Camera_Controller(cameraModel);


    }

}
