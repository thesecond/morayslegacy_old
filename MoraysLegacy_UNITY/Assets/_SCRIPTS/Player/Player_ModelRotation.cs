﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_ModelRotation : MonoBehaviour {

    [SerializeField]
    private Rigidbody _rigidBody;

    [SerializeField]
    [Range(0, 1)]
    private float lerpSpeed;
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 direction = _rigidBody.velocity;
        direction = new Vector3(direction.x, 0f, direction.z);

        if (direction.magnitude > 0.1f)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), lerpSpeed);
        }
	}
}
