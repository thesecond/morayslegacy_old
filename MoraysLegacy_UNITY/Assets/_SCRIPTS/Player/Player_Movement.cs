﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Rigidbody))]
public class Player_Movement : MonoBehaviour {

    [Header("Movement")]
    [SerializeField]
    private float _maxWalkSpeed;
    [SerializeField]
    private float _maxSprintSpeed;
    private float _currentSpeed;
    [SerializeField]
    [Range(0,1)]
    private float _acceleration;
    private Vector3 currentForceVector;
    


    private Rigidbody _rigidBody;

    private Vector2 _normalizedInputVector;

    private enum MovementState {Standing, Walking, Sprinting};
    [SerializeField]
    private MovementState _movementState;

    [SerializeField]
    private Transform cameraRotationCenter;

    private enum JumpState { Grounded, InAir};
    [Header("Jump")]
    [SerializeField]
    private JumpState _jumpState;
    [SerializeField]
    private float _jumpForce;
    private float _currentJumpForce;
    [SerializeField]
    private float _fallGravityMultiplier;


    [SerializeField]
    private LayerMask _checkJumpLayerMask;
    private float _checkJumpSphereRadius = 0.1f;
    
    
	void Start ()
    {
        _rigidBody = GetComponent<Rigidbody>();
	}
	
	void Update ()
    {
        _normalizedInputVector = GetNormalizedInputVector();

        if(_normalizedInputVector.magnitude < 0.02f)
        {
            _movementState = MovementState.Standing;
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            _movementState = MovementState.Sprinting;
        }
        else
        {
            _movementState = MovementState.Walking;
        }

        //Handle Jump
        if (Input.GetKeyDown(KeyCode.Space) && _jumpState == JumpState.Grounded)
        {
            _currentJumpForce = _jumpForce;
        }
        else
        {
            _currentJumpForce = 0f;
        }
	}

    Vector2 GetNormalizedInputVector()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        return new Vector2(x,y).normalized;
    }

    void FixedUpdate()
	{
        Vector3 desiredForceVector = new Vector3(_normalizedInputVector.x, 0f, _normalizedInputVector.y);

        float desiredMovementSpeed = 0f;

        switch (_movementState)
        {
            case MovementState.Walking:
                desiredMovementSpeed = _maxWalkSpeed;
                break;
            case MovementState.Sprinting:
                desiredMovementSpeed = _maxSprintSpeed;
                break;
        }

        desiredForceVector *= (desiredMovementSpeed * Time.fixedDeltaTime);
        currentForceVector= Vector3.Lerp(currentForceVector, desiredForceVector, _acceleration);

        //Handle jump Force
        float yForce = Mathf.Lerp(_rigidBody.velocity.y, _currentJumpForce, Mathf.Clamp01(_currentJumpForce)) + _currentJumpForce;
        if (_rigidBody.velocity.y < 0f)
        {
            yForce -= _fallGravityMultiplier;
        }

        currentForceVector = new Vector3(currentForceVector.x, yForce, currentForceVector.z);
        _rigidBody.velocity = Quaternion.Euler(0f, cameraRotationCenter.rotation.eulerAngles.y, 0f) * currentForceVector;

        if(_currentJumpForce > 0f)
        {
            _currentJumpForce = 0f;
        }

        //Check if Grounded
        if (Physics.CheckSphere(transform.position,_checkJumpSphereRadius, _checkJumpLayerMask))
        {
            _jumpState = JumpState.Grounded;
        }
        else
        {
            _jumpState = JumpState.InAir;
        }

	}
}
