﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControllerBase<ModelType> where ModelType : class
{
    private ModelType _model;
    public ModelType Model
    {
        get { return _model; }
        set
        {
            OnModelChanged(_model, _model = value);
        }
    }

    public Action EventModelChanged = delegate { };


    protected virtual void OnModelChanged(ModelType oldModel, ModelType newModel)
    {
        Refresh();
        EventModelChanged();
    }

    protected virtual void Refresh()
    {

    }
}

/*
public abstract class Controller<ModelType> : ControllerBase<ModelType> where ModelType : class
{

}
*/
