﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewBase<ControllerType> : MonoBehaviour {

    private ControllerType _controller;
    public ControllerType Controller
    {
        get { return _controller; }
        set
        {
            OnControllerChanged(_controller, _controller = value);
        }
    }

    protected virtual void OnControllerChanged(ControllerType oldController, ControllerType newController)
    {
        Refresh();
    }

    protected virtual void Refresh()
    {

    }
}

public abstract class View<ControllerType> : ViewBase<ControllerType>
{

}

