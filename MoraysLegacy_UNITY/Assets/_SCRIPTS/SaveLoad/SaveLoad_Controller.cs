﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Xml.Serialization;

public class SaveLoad_Controller  {

    //    private SaveLoad_Model _model;

    public string savePath = Application.persistentDataPath + "/SaveML/saveFile.mlsf";

    public void Save(SaveLoad_Model modelToSave)
    {
        //        BinaryFormatter bf = new BinaryFormatter();

        XmlSerializer serializer = new XmlSerializer(typeof(SaveLoad_Model));

        FileStream file;

        if (File.Exists(savePath))
        {
            file = File.Open(savePath, FileMode.Open);
        } else {
            file = File.Create(savePath);
        }

        XmlWriter writer = new XmlTextWriter(file, Encoding.Unicode);
        serializer.Serialize(writer, modelToSave);
        //        bf.Serialize(file, modelToSave);

        writer.Close();
        file.Close();
        Debug.Log("Saved at: " + savePath);

    }

    public SaveLoad_Model Load()
    {
        Debug.Log("Trying to Load...");
        if (File.Exists(savePath))
        {
            //BinaryFormatter bf = new BinaryFormatter();

            XmlSerializer serializer = new XmlSerializer(typeof(SaveLoad_Model));            

            FileStream file = File.Open(savePath, FileMode.Open);
            XmlReader reader = XmlReader.Create(file);

            //SaveLoad_Model model = (SaveLoad_Model)bf.Deserialize(file);
            SaveLoad_Model model = (SaveLoad_Model)serializer.Deserialize(reader);

            file.Close();
            Debug.Log("Loading successful!");
            return model;
        } else {
            Debug.LogWarning("File does not exist!");
        }
        return null;
    }

}
