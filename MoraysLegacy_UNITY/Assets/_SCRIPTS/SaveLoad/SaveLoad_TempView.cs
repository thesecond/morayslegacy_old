﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoad_TempView : View<SaveLoad_Controller> {



    // Use this for initialization
	void Start () 
    {
        Controller = GameManager.staticInstance.saveLoadController;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Save
        if (Input.GetKeyDown(KeyCode.F6))
        {
            //Create Model
            SaveLoad_Model model = new SaveLoad_Model();

            model.CameraModel = GameManager.staticInstance.cameraController.Model;

            //Save Manager
            Controller.Save(model);

        }

        //Load
        if (Input.GetKeyDown(KeyCode.F7))
        {
            SaveLoad_Model model = Controller.Load();

            GameManager.staticInstance.cameraController.Model = model.CameraModel;

        }

	}
}
