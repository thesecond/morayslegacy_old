﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Camera_Model
{

    public float mouseSensitivity;
    public bool invertYAxis;
    public ShoulderState shoulderState;

}

//ENUMS
[System.Serializable]
public enum ShoulderState { Right, Left };
