﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_ShoulderView : View<Camera_Controller>
{

    [SerializeField]
    private float offset = 0.6f;

    private ShoulderState _shoulder;
    public ShoulderState Shoulder
    {
        get { return _shoulder; }
        set
        {
            _shoulder = value;
            SetDesiredXPos(value);
        }
    }

    [SerializeField]
    [Range(0,1)]
    private float changeLerpSpeed;
    
    private float desiredXPos;


    private void Start()
    {
        Controller = GameManager.staticInstance.cameraController;
        Shoulder = Controller.GetShoulder();
        Controller.EventShoulderChanged += SetDesiredXPos;
    }

    private void OnDestroy()
    {
        Controller.EventShoulderChanged -= SetDesiredXPos;
    }

    private void SetDesiredXPos(ShoulderState state)
    {
        switch (state)
        {
            case ShoulderState.Left:
                desiredXPos = -offset;
                break;
            case ShoulderState.Right:
                desiredXPos = offset;
                break;
        }
        StopCoroutine("ChangeShoulder");
        StartCoroutine("ChangeShoulder");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            Controller.ChangeShoulder();
        }
    }

    private void JumpToPosition(float pos)
    {
        SetLocalXPosition(desiredXPos = pos);
    }

    private void SetLocalXPosition(float pos)
    {
        transform.localPosition = new Vector3(pos, 0f, 0f);
    }

    public IEnumerator ChangeShoulder()
    {
        while(transform.localPosition.x != desiredXPos)
        {

            SetLocalXPosition(Mathf.Lerp(transform.localPosition.x, desiredXPos, changeLerpSpeed));
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }


}
