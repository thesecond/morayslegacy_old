﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Camera_Controller : ControllerBase<Camera_Model> {

    

    public float GetMouseSensitivity()
    {
        return Model.mouseSensitivity;
    }

    public ShoulderState GetShoulder()
    {
        return Model.shoulderState;
    }

    public Action<ShoulderState> EventShoulderChanged = delegate { };
    public void ChangeShoulder()
    {
        Model.shoulderState = Model.shoulderState != ShoulderState.Left ? ShoulderState.Left : ShoulderState.Right;
        EventShoulderChanged(Model.shoulderState);
    }


    public Camera_Controller(Camera_Model model)
    {
        Model = model;
    }

    protected override void OnModelChanged(Camera_Model oldModel, Camera_Model newModel)
    {
        base.OnModelChanged(oldModel, newModel);
    }
}
