﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_RotationView : View<Camera_Controller> {


    float mouseSensitivity;

    float yValue;
    float xValue;


    private void Start()
    {
        Controller = GameManager.staticInstance.cameraController;
        mouseSensitivity = Controller.GetMouseSensitivity();
        Controller.EventModelChanged += UpdateValues;
    }

    private void OnDestroy()
    {
        Controller.EventModelChanged -= UpdateValues;
    }

    private void UpdateValues()
    {
        mouseSensitivity = Controller.GetMouseSensitivity();
    }

    protected override void OnControllerChanged(Camera_Controller oldController, Camera_Controller newController)
    {
        if(newController != null)
        {
            mouseSensitivity = newController.GetMouseSensitivity();
        }
        else if(oldController != null)
        {
            Debug.LogWarning("No new Camera Controller found! Using old value!");
            mouseSensitivity = oldController.GetMouseSensitivity();
        }
        else
        {
            Debug.LogError("No Controllers found for Camera, using default Value of 200!");
            mouseSensitivity = 200f;
        }

    }

    // Update is called once per frame
    void Update ()
    {
        Vector2 mouseInput = GetMouseInput();

        yValue += mouseInput.x * Time.deltaTime * mouseSensitivity;
        xValue -= mouseInput.y * Time.deltaTime * mouseSensitivity;

        xValue = Mathf.Clamp(xValue, -80f, 80f);

        Vector3 rotationVector = new Vector3(xValue,yValue,0f);

        transform.rotation = Quaternion.Euler(rotationVector);
	}

    Vector2 GetMouseInput()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");

        return new Vector2(x, y);
    }
}
